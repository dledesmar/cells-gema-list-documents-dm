{
  const {
    html,
  } = Polymer;
  /**
    `<cells-gema-list-documents-dm>` Description.

    Example:

    ```html
    <cells-gema-list-documents-dm></cells-gema-list-documents-dm>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-gema-list-documents-dm | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsGemaListDocumentsDm extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-gema-list-documents-dm';
    }

    static get properties() {
      return {
        /**
         * The URL of HOST
         * @type {String}
         * @default ''
         */
        host: {
          type: String,
          value: ''
        },
        /**
         * Property to set the base Path to call the service
         * @type {String}
         * @default ''
         */
        basePath: {
          type: String,
          value: ''
        },
        /**
         * Property to set the base Path to call the service, of API implemented for the Account product
         * @type {String}
         * @default ''
         */
        handleAs: {
          type: String,
          value: ''
        },
        /**
         * The path to call the service
         * @type {String}
         * @default ''
         */
        path: {
          type: String,
          value: ''
        },
        /**
         * The method to call
         * @type {String}
         * @default 'GET'
         */
        method: {
          type: String,
          value: 'GET'
        },
        /**
         * The config of HEADERS
         * @type {String}
         * @default ''
         */
        headers: {
          type: String,
          value: ''
        },
        /**
         * Params to send in request
         * @type {String}
         * @default ''
         */
        params: {
          type: String,
          value: ''
        },
        /**
         * The body of request
         * @type {String}
         * @default ''
         */
        body: {
          type: String,
          value: ''
        },
        /**
         * The tsec of current session
         * @type {String}
         * @default ''
         */
        tsec: {
          type: String,
          value: ''
        },
        /**
         * Configure if response is Stub/Mock
         * @type {Boolean}
         * @default false
         */
        isMock: {
          type: Boolean,
          value: false
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-gema-list-documents-dm-styles cells-gema-list-documents-dm-shared-styles"></style>
      <slot></slot>
      <gema-cells-generic-dp
        id="myGenericProvider"
        host="[[host]]"
        path="[[path]]"
        params$="[[params]]"
        body$="[[body]]"
        method="[[method]]"
        headers$="[[headers]]"
        handle-as$="[[handleAs]]"
        native="true"
      </gema-cells-generic-dp>          
      `;
    }

    ready() {
      super.ready();
      this._getTsec();
      this.set('_myGenericProvider', this.$.myGenericProvider);
    }

    _getTsec() {
      const tsec = sessionStorage.getItem('tsec');
      if (tsec) {
        this.set('tsec', tsec);
      }
    }

    /**
     * Function to adds new headers when is required
     * @param {Object} newHeadValues The object of headers
     * @example {tsec: 'abcd', Content-Type: 'application/json'}
     */
    _addHeaders(newHeadValues) {
      this.set('headers', JSON.stringify(newHeadValues));
    }

    /**
     * Function to clean properties of a DM.
     * Returns the parameters to their default value
     */
    _cleanParameters() {
      this.set('path', '');
      this.set('method', 'GET');
      this.set('params', '');
      this.set('body', '');
      this.set('handleAs', '');
      this.set('accountId', '');
      this.set('headers', '{"tsec":"' + sessionStorage.getItem('tsec') + '"}');
    }

    _existData(value) {
      let status = false;
      if (value !== undefined && value !== null && !Number.isNaN(value) && value !== '') {
        status = true;
      }
      return status;
    }

    /**
     * Function to return response to channel
     * @param {String} eventName The name of CustomEvent
     * @param {*} detailValue The returned value
     */
    _returnResponse(eventName, detailValue) {
      this.dispatchEvent(new CustomEvent(eventName, {
        composed: true,
        bubbles: true,
        detail: detailValue
      }));
    }

    grantingTicket(params, body, handle) {
      this._cleanParameters();
      this.set('path', params);
      this.set('method', 'POST');
      this.set('body', body);
      this.set('handleAs', handle);
      //this._addHeaders({ tsec: this.tsec, [`Content-Type`]: 'application/json' });
      if (this._existData(this.host)) {
        const promise = this._myGenericProvider.generateRequest().completes;
        promise.then(response => {
          this._returnResponse('granting-ticket-event', {data:response, status:'success'});
        }).catch(reason => {
          this._returnResponse('granting-ticket-event', {data:reason, status:'error'});
        });
      }
    }

    endPointGet(path, params, tsec, handle) {
      this._cleanParameters();
      this.set('path', path);
      this.set('method', 'GET');
      this.set('params', params);
      this.set('tsec', tsec);
      this.set('handleAs', handle);
      this._addHeaders({ tsec: this.tsec, [`Content-Type`]: 'application/json' });
      //this._addHeaders({ tsec: this.tsec, [`Content-Type`]: 'application/json' });
      if (this._existData(this.host)) {
        const promise = this._myGenericProvider.generateRequest().completes;
        promise.then(response => {
          this._returnResponse('endPointGet-event', {data:response, status:'success'});
        }).catch(reason => {
          this._returnResponse('endPointGet-event', {data:reason, status:'error'});
        });
      }
    }

    endPoint(body, handle) {
      this.set('body', body);
      this.set('method', 'POST');
      this.set('handleAs', handle);
      if (this._existData(this.host)) {
        const promise = this._myGenericProvider.generateRequest().completes;
        promise.then(response => {
          this._returnResponse('endPoint-event', {data:response, status:'success'});
        }).catch(reason => {
          this._returnResponse('endPoint-event', {data:reason, status:'error'});
        });
      }
    }


  }

  customElements.define(CellsGemaListDocumentsDm.is, CellsGemaListDocumentsDm);
}